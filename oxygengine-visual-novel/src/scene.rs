use crate::{background::BackgroundStyle, Position};
use anim::{animation::Animation, transition::Transition};
use core::{prefab::Prefab, Scalar};
use serde::{Deserialize, Serialize};

pub type ActiveScene = Transition<Option<String>>;

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Scene {
    #[serde(default)]
    pub name: String,
    #[serde(default)]
    pub background_style: BackgroundStyle,
    #[serde(default)]
    pub camera_position: Animation<Position>,
    #[serde(default)]
    pub camera_rotation: Animation<Scalar>,
}

impl Prefab for Scene {}

impl Scene {
    pub fn in_progress(&self) -> bool {
        self.background_style.in_progress()
            || self.camera_position.in_progress()
            || self.camera_rotation.in_progress()
    }

    pub fn process(&mut self, delta_time: Scalar) {
        self.background_style.process(delta_time);
        self.camera_position.process(delta_time);
        self.camera_rotation.process(delta_time);
    }
}
