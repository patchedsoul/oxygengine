use crate::{Color, Position, Scale};
use anim::{
    animation::{Animation, AnimationProgression},
    transition::Transition,
};
use core::{prefab::Prefab, Scalar};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

pub type CharacterStyle = Transition<String>;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Character {
    name: String,
    /// {style name: image name}
    pub styles: HashMap<String, String>,
    #[serde(default)]
    style: CharacterStyle,
    #[serde(default)]
    visibility: Animation<Scalar>,
    #[serde(default)]
    name_color: Animation<Color>,
    /// position in screen space percentage.
    #[serde(default)]
    position: Animation<Position>,
    /// alignment tells where in the image space pivot is located (percentage of image size).
    #[serde(default)]
    alignment: Animation<Position>,
    #[serde(default)]
    rotation: Animation<Scalar>,
    #[serde(default)]
    scale: Animation<Scale>,
    #[serde(skip)]
    pub(crate) dirty: bool,
}

impl Prefab for Character {
    fn post_from_prefab(&mut self) {
        self.dirty = true;
    }
}

impl Default for Character {
    fn default() -> Self {
        Self {
            name: Default::default(),
            styles: Default::default(),
            style: Default::default(),
            visibility: Animation::instant(1.0, AnimationProgression::Duration(1.0)),
            name_color: Animation::instant(
                (1.0, 1.0, 1.0).into(),
                AnimationProgression::Duration(1.0),
            ),
            position: Default::default(),
            alignment: Default::default(),
            rotation: Default::default(),
            scale: Animation::instant((1.0, 1.0).into(), AnimationProgression::Duration(1.0)),
            dirty: true,
        }
    }
}

impl Character {
    pub fn new(name: &str) -> Self {
        Self {
            name: name.to_owned(),
            ..Default::default()
        }
    }

    pub fn is_dirty(&self) -> bool {
        self.dirty
            || self.style.in_progress()
            || self.visibility.in_progress()
            || self.name_color.in_progress()
            || self.position.in_progress()
            || self.alignment.in_progress()
            || self.rotation.in_progress()
            || self.scale.in_progress()
    }

    pub fn rebuild(&mut self) {
        self.dirty = true;
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn set_name(&mut self, value: String) {
        self.name = value;
        self.dirty = true;
    }

    /// (style name, image name)
    pub fn styles(&self) -> impl Iterator<Item = (&str, &str)> {
        self.styles.iter().map(|(k, v)| (k.as_str(), v.as_str()))
    }

    /// (from style, phase, to style)
    pub fn style(&self) -> (&str, Scalar, &str) {
        let from = self.style.from().as_str();
        let to = self.style.to().as_str();
        let phase = self.style.phase();
        (from, phase, to)
    }

    pub fn style_transition(&self) -> &CharacterStyle {
        &self.style
    }

    pub fn style_transition_mut(&mut self) -> &mut CharacterStyle {
        &mut self.style
    }

    pub fn set_style(&mut self, value: String) {
        self.style.set(value);
    }

    pub fn visibility(&self) -> Scalar {
        self.visibility.current().unwrap_or(1.0)
    }

    pub fn visibility_anim(&self) -> &Animation<Scalar> {
        &self.visibility
    }

    pub fn visibility_anim_mut(&mut self) -> &mut Animation<Scalar> {
        &mut self.visibility
    }

    pub fn set_visibility(&mut self, value: Scalar) {
        self.visibility.animate_linear_into(value)
    }

    pub fn hide(&mut self) {
        self.set_visibility(0.0);
    }

    pub fn show(&mut self) {
        self.set_visibility(1.0);
    }

    pub fn name_color(&self) -> Color {
        self.name_color.current().unwrap_or(Color(1.0, 1.0, 1.0))
    }

    pub fn name_color_anim(&self) -> &Animation<Color> {
        &self.name_color
    }

    pub fn name_color_anim_mut(&mut self) -> &mut Animation<Color> {
        &mut self.name_color
    }

    pub fn set_name_color(&mut self, value: Color) {
        self.name_color.animate_linear_into(value)
    }

    pub fn position(&self) -> Position {
        self.position.current().unwrap_or(Default::default())
    }

    pub fn position_anim(&self) -> &Animation<Position> {
        &self.position
    }

    pub fn position_anim_mut(&mut self) -> &mut Animation<Position> {
        &mut self.position
    }

    pub fn set_position(&mut self, value: Position) {
        self.position.animate_linear_into(value)
    }

    pub fn alignment(&self) -> Position {
        self.alignment.current().unwrap_or(Default::default())
    }

    pub fn alignment_anim(&self) -> &Animation<Position> {
        &self.alignment
    }

    pub fn alignment_anim_mut(&mut self) -> &mut Animation<Position> {
        &mut self.alignment
    }

    pub fn set_alignment(&mut self, value: Position) {
        self.alignment.animate_linear_into(value)
    }

    pub fn rotation(&self) -> Scalar {
        self.rotation.current().unwrap_or(0.0)
    }

    pub fn rotation_anim(&self) -> &Animation<Scalar> {
        &self.rotation
    }

    pub fn rotation_anim_mut(&mut self) -> &mut Animation<Scalar> {
        &mut self.rotation
    }

    pub fn set_rotation(&mut self, value: Scalar) {
        self.rotation.animate_linear_into(value)
    }

    pub fn scale(&self) -> Scale {
        self.scale.current().unwrap_or(Position(1.0, 1.0))
    }

    pub fn scale_anim(&self) -> &Animation<Scale> {
        &self.scale
    }

    pub fn scale_anim_mut(&mut self) -> &mut Animation<Scale> {
        &mut self.scale
    }

    pub fn set_scale(&mut self, value: Scale) {
        self.scale.animate_linear_into(value)
    }

    pub fn process(&mut self, delta_time: Scalar) {
        self.style.process(delta_time);
        self.visibility.process(delta_time);
        self.name_color.process(delta_time);
        self.position.process(delta_time);
        self.alignment.process(delta_time);
        self.rotation.process(delta_time);
        self.scale.process(delta_time);
    }
}
