use oxygengine::prelude::*;

#[derive(Debug, Default)]
pub struct GameState {
    camera_ui: Option<Entity>,
}

impl State for GameState {
    fn on_enter(&mut self, world: &mut World) {
        // instantiate world objects from scene prefab.
        // world
        //     .write_resource::<PrefabManager>()
        //     .instantiate_world("scene", world)
        //     .unwrap();
        let assets = world.read_resource::<AssetsDatabase>();
        let asset = assets
            .asset_by_path("vn-story://story.yaml")
            .expect("trying to use not loaded VN story asset");
        let asset = asset
            .get::<VnStoryAsset>()
            .expect("trying to use non-vn-story asset");
        let mut story = asset.get().clone();
        story
            .run_chapter("Main")
            .expect("could not run main chapter");
        world
            .write_resource::<VnStoryManager>()
            .register_story("main", story);
    }

    fn on_process(&mut self, world: &mut World) -> StateChange {
        if let Some(camera) = self.camera_ui {
            let input = &world.read_resource::<InputController>();
            if input.trigger_or_default("mouse-left").is_pressed() {
                let x = input.axis_or_default("mouse-x");
                let y = input.axis_or_default("mouse-y");
                let point = [x, y].into();

                if let Some(pos) = world
                    .read_resource::<CompositeCameraCache>()
                    .screen_to_world_space(camera, point)
                {
                    if world
                        .read_resource::<CompositeUiInteractibles>()
                        .does_rect_contains_point("panel", pos)
                    {
                        info!("=== PANEL GOT CLICKED!");
                    }
                }
            }
        } else {
            self.camera_ui = entity_find_world("camera_ui", world);
        }

        StateChange::None
    }
}
