use oxygengine_composite_renderer::{
    component::{
        CompositeCamera, CompositeCameraAlignment, CompositeRenderAlpha, CompositeRenderLayer,
        CompositeRenderable, CompositeScalingMode, CompositeScalingTarget, CompositeTransform,
        CompositeUiElement, CompositeVisibility, UiElementType, UiImage, UiMargin,
    },
    composite_renderer::{Command, Image, Renderable, Text},
    math::{Color, Mat2d, Vec2},
    resource::CompositeCameraCache,
};
use oxygengine_core::{
    app::AppBuilder,
    ecs::{
        world::{Builder, EntitiesRes},
        Component, Entity, Join, LazyUpdate, Read, System, VecStorage, Write, WriteStorage,
    },
    hierarchy::{Name, Tag},
    prefab::{Prefab, PrefabError, PrefabManager, PrefabProxy},
    state::StateToken,
    Scalar,
};
use oxygengine_visual_novel::resource::VnStoryManager;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
#[cfg(not(feature = "scalar64"))]
use std::f32::consts::PI;
#[cfg(feature = "scalar64")]
use std::f64::consts::PI;

pub mod prelude {
    pub use crate::*;
}

pub fn bundle_installer<'a, 'b>(
    builder: &mut AppBuilder<'a, 'b>,
    vm_rendering_config: VnRenderingConfig,
) {
    builder.install_resource(VnRenderingManager::new(vm_rendering_config));
    builder.install_system(
        ApplyVisualNovelToCompositeRenderer,
        "apply-visual-novel-to-composite-renderer",
        &[],
    );
}

pub fn prefabs_installer(prefabs: &mut PrefabManager) {
    prefabs.register_component_factory_proxy::<PositionCameraAlignment, PositionCameraAlignmentPrefabProxy>("PositionCameraAlignment");
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum VnRenderingOverlayStyle {
    Color(Color),
    Image(String),
}

impl Default for VnRenderingOverlayStyle {
    fn default() -> Self {
        Self::Color(Color::black())
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct VnRenderingConfig {
    pub world_camera_scaling_target: CompositeScalingTarget,
    pub world_camera_resolution: Scalar,
    pub world_camera_layer: usize,
    pub ui_camera_scaling_target: CompositeScalingTarget,
    pub ui_camera_resolution: Scalar,
    pub ui_camera_layer: usize,
    pub overlay_style: VnRenderingOverlayStyle,
    pub hide_on_story_pause: bool,
    pub ui_component_template: Option<CompositeUiElement>,
    pub ui_dialogue_default_theme: Option<String>,
    pub ui_dialogue_panel_path: String,
    pub ui_dialogue_text_path: String,
    pub ui_dialogue_name_path: String,
    pub ui_dialogue_skip_path: String,
    pub forced_constant_refresh: bool,
}

impl Default for VnRenderingConfig {
    fn default() -> Self {
        Self {
            world_camera_scaling_target: Default::default(),
            world_camera_resolution: 1080.0,
            world_camera_layer: 1000,
            ui_camera_scaling_target: Default::default(),
            ui_camera_resolution: 1080.0,
            ui_camera_layer: 1001,
            overlay_style: Default::default(),
            hide_on_story_pause: false,
            ui_component_template: None,
            ui_dialogue_default_theme: None,
            ui_dialogue_panel_path: "panel".to_owned(),
            ui_dialogue_text_path: "text".to_owned(),
            ui_dialogue_name_path: "name".to_owned(),
            ui_dialogue_skip_path: "skip".to_owned(),
            forced_constant_refresh: false,
        }
    }
}

#[derive(Debug, Clone)]
pub struct VnStoryRenderer {
    world_camera: Entity,
    ui_camera: Entity,
    characters: HashMap<String, Entity>,
    background: Entity,
    dialogue_ui_element: Entity,
    overlay: Entity,
}

#[derive(Debug, Default, Clone)]
pub struct VnRenderingManager {
    config: VnRenderingConfig,
    stories: HashMap<String, VnStoryRenderer>,
}

impl VnRenderingManager {
    pub fn new(config: VnRenderingConfig) -> Self {
        Self {
            config,
            stories: Default::default(),
        }
    }
}

impl From<VnRenderingConfig> for VnRenderingManager {
    fn from(config: VnRenderingConfig) -> Self {
        Self::new(config)
    }
}

#[derive(Debug, Clone, Copy)]
pub struct PositionCameraAlignment(pub Entity, pub Vec2);

impl Component for PositionCameraAlignment {
    type Storage = VecStorage<Self>;
}

impl PrefabProxy<PositionCameraAlignmentPrefabProxy> for PositionCameraAlignment {
    fn from_proxy_with_extras(
        proxy: PositionCameraAlignmentPrefabProxy,
        named_entities: &HashMap<String, Entity>,
        _: StateToken,
    ) -> Result<Self, PrefabError> {
        if let Some(entity) = named_entities.get(&proxy.0) {
            Ok(Self(*entity, proxy.1))
        } else {
            Err(PrefabError::Custom(format!(
                "Could not find entity named: {}",
                proxy.0
            )))
        }
    }
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct PositionCameraAlignmentPrefabProxy(String, Vec2);

impl Prefab for PositionCameraAlignmentPrefabProxy {}

#[derive(Debug, Default)]
pub struct ApplyVisualNovelToCompositeRenderer;

impl<'s> System<'s> for ApplyVisualNovelToCompositeRenderer {
    type SystemData = (
        Read<'s, EntitiesRes>,
        Read<'s, LazyUpdate>,
        Read<'s, VnStoryManager>,
        Read<'s, CompositeCameraCache>,
        Write<'s, VnRenderingManager>,
        WriteStorage<'s, CompositeRenderable>,
        WriteStorage<'s, CompositeVisibility>,
        WriteStorage<'s, CompositeRenderAlpha>,
        WriteStorage<'s, CompositeTransform>,
        WriteStorage<'s, PositionCameraAlignment>,
    );

    fn run(
        &mut self,
        (
            entities,
            lazy_update,
            stories,
            camera_cache,
            mut rendering,
            mut renderables,
            mut visibilities,
            mut alphas,
            mut transforms,
            mut alignments,
        ): Self::SystemData,
    ) {
        for id in stories.lately_unregistered() {
            if let Some(story) = rendering.stories.remove(id) {
                drop(entities.delete(story.world_camera));
                drop(entities.delete(story.ui_camera));
                for entity in story.characters.values() {
                    drop(entities.delete(*entity));
                }
                drop(entities.delete(story.background));
                drop(entities.delete(story.dialogue_ui_element));
                drop(entities.delete(story.overlay));
            }
        }

        for id in stories.lately_registered() {
            if let Some(story) = stories.get(id) {
                let world_camera = lazy_update
                    .create_entity(&entities)
                    .with(Name(format!("vn-camera-world-{}", id).into()))
                    .with(
                        CompositeCamera::with_scaling_target(
                            CompositeScalingMode::CenterAspect,
                            rendering.config.world_camera_scaling_target,
                        )
                        .tag(format!("vn-world-{}", id).into()),
                    )
                    .with(CompositeVisibility(false))
                    .with(CompositeRenderLayer(rendering.config.world_camera_layer))
                    .with(CompositeTransform::scale(
                        rendering.config.world_camera_resolution.into(),
                    ))
                    .build();
                let ui_camera = lazy_update
                    .create_entity(&entities)
                    .with(Name(format!("vn-camera-ui-{}", id).into()))
                    .with(
                        CompositeCamera::with_scaling_target(
                            CompositeScalingMode::Aspect,
                            rendering.config.ui_camera_scaling_target,
                        )
                        .tag(format!("vn-ui-{}", id).into()),
                    )
                    .with(CompositeVisibility(false))
                    .with(CompositeRenderLayer(rendering.config.ui_camera_layer))
                    .with(CompositeTransform::scale(
                        rendering.config.ui_camera_resolution.into(),
                    ))
                    .build();
                let characters = story
                    .characters()
                    .map(|(n, _)| {
                        let entity = lazy_update
                            .create_entity(&entities)
                            .with(Tag(format!("vn-world-{}", id).into()))
                            .with(Name(format!("vn-character-{}-{}", id, n).into()))
                            .with(CompositeRenderAlpha(0.0))
                            .with(CompositeRenderLayer(1))
                            .with(CompositeRenderable(().into()))
                            .with(PositionCameraAlignment(world_camera, 0.0.into()))
                            .with(CompositeTransform::default())
                            .build();
                        (n.to_owned(), entity)
                    })
                    .collect::<HashMap<_, _>>();
                let background = lazy_update
                    .create_entity(&entities)
                    .with(Tag(format!("vn-world-{}", id).into()))
                    .with(Name(format!("vn-background-{}", id).into()))
                    .with(CompositeRenderable(().into()))
                    .with(CompositeTransform::default())
                    .build();
                let ui_element = if let Some(mut component) =
                    rendering.config.ui_component_template.clone()
                {
                    component.camera_name = format!("vn-camera-ui-{}", id).into();
                    if let Some(child) =
                        component.find_mut(&rendering.config.ui_dialogue_panel_path)
                    {
                        child.interactive = Some(format!("vn-ui-panel-{}", id).into());
                    }
                    if let Some(child) = component.find_mut(&rendering.config.ui_dialogue_text_path)
                    {
                        child.interactive = Some(format!("vn-ui-text-{}", id).into());
                    }
                    if let Some(child) = component.find_mut(&rendering.config.ui_dialogue_name_path)
                    {
                        child.interactive = Some(format!("vn-ui-name-{}", id).into());
                    }
                    if let Some(child) = component.find_mut(&rendering.config.ui_dialogue_skip_path)
                    {
                        child.interactive = Some(format!("vn-ui-skip-{}", id).into());
                    }
                    component
                } else {
                    let mut text = CompositeUiElement::default();
                    text.id = Some(rendering.config.ui_dialogue_text_path.clone().into());
                    text.theme = rendering
                        .config
                        .ui_dialogue_default_theme
                        .as_ref()
                        .map(|theme| format!("{}@text", theme).into());
                    text.interactive = Some(format!("vn-ui-text-{}", id).into());
                    text.element_type = UiElementType::Text(
                        Text::new_owned("Verdana".to_owned(), "<TEXT>".to_owned())
                            .color(Color::white()),
                    );
                    text.padding = UiMargin {
                        left: 64.0,
                        right: 64.0,
                        top: 180.0,
                        bottom: 0.0,
                    };
                    text.left_anchor = 0.0;
                    text.right_anchor = 1.0;
                    text.top_anchor = 0.0;
                    text.bottom_anchor = 1.0;
                    let mut name = CompositeUiElement::default();
                    name.id = Some(rendering.config.ui_dialogue_name_path.clone().into());
                    name.theme = rendering
                        .config
                        .ui_dialogue_default_theme
                        .as_ref()
                        .map(|theme| format!("{}@name", theme).into());
                    name.element_type = UiElementType::Text(
                        Text::new_owned("Verdana".to_owned(), "<NAME>".to_owned())
                            .color(Color::white()),
                    );
                    name.interactive = Some(format!("vn-ui-name-{}", id).into());
                    name.padding = UiMargin {
                        left: 32.0,
                        right: 32.0,
                        top: 8.0,
                        bottom: 8.0,
                    };
                    name.left_anchor = 0.0;
                    name.right_anchor = 1.0;
                    name.top_anchor = 0.0;
                    name.bottom_anchor = 0.0;
                    name.alignment = (0.0, 0.0).into();
                    name.fixed_height = Some(64.0);
                    let mut panel = CompositeUiElement::default();
                    panel.id = Some(rendering.config.ui_dialogue_panel_path.clone().into());
                    panel.theme = rendering
                        .config
                        .ui_dialogue_default_theme
                        .as_ref()
                        .map(|theme| format!("{}@panel", theme).into());
                    panel.interactive = Some(format!("vn-ui-panel-{}", id).into());
                    panel.element_type = UiElementType::Image(Box::new(UiImage::default()));
                    panel.padding = UiMargin {
                        left: 128.0,
                        right: 128.0,
                        top: 0.0,
                        bottom: 32.0,
                    };
                    panel.left_anchor = 0.0;
                    panel.right_anchor = 1.0;
                    panel.top_anchor = 1.0;
                    panel.bottom_anchor = 1.0;
                    panel.alignment = (0.0, 1.0).into();
                    panel.fixed_height = Some(256.0);
                    panel.children = vec![text, name];
                    let mut root = CompositeUiElement::default();
                    root.camera_name = format!("vn-camera-ui-{}", id).into();
                    root.element_type = UiElementType::None;
                    root.left_anchor = 0.0;
                    root.right_anchor = 1.0;
                    root.top_anchor = 0.0;
                    root.bottom_anchor = 1.0;
                    root.children = vec![panel];
                    root
                };
                let dialogue_ui_element = lazy_update
                    .create_entity(&entities)
                    .with(Tag(format!("vn-ui-{}", id).into()))
                    .with(Name(format!("vn-dialogue-{}", id).into()))
                    .with(CompositeRenderAlpha(0.0))
                    .with(CompositeRenderable(().into()))
                    .with(ui_element)
                    .with(CompositeTransform::default())
                    .build();
                let overlay_renderable = match &rendering.config.overlay_style {
                    VnRenderingOverlayStyle::Color(color) => {
                        Renderable::FullscreenRectangle(*color)
                    }
                    VnRenderingOverlayStyle::Image(image) => {
                        Image::new_owned(image.to_owned()).align(0.5.into()).into()
                    }
                };
                let overlay = lazy_update
                    .create_entity(&entities)
                    .with(Tag(format!("vn-ui-{}", id).into()))
                    .with(Name(format!("vn-overlay-{}", id).into()))
                    .with(CompositeVisibility(false))
                    .with(CompositeRenderAlpha(0.0))
                    .with(CompositeRenderLayer(1))
                    .with(CompositeRenderable(overlay_renderable))
                    .with(CompositeCameraAlignment(0.0.into()))
                    .with(CompositeTransform::default())
                    .build();
                rendering.stories.insert(
                    id.to_owned(),
                    VnStoryRenderer {
                        world_camera,
                        ui_camera,
                        characters,
                        background,
                        dialogue_ui_element,
                        overlay,
                    },
                );
            }
        }

        for (story_name, story_data) in &rendering.stories {
            if let Some(story) = stories.get(story_name) {
                let show = !story.is_paused() || !rendering.config.hide_on_story_pause;
                if let Some(visibility) = visibilities.get_mut(story_data.world_camera) {
                    visibility.0 = show;
                }
                if let Some(visibility) = visibilities.get_mut(story_data.ui_camera) {
                    visibility.0 = show;
                }
                if !show {
                    continue;
                }

                let refresh = rendering.config.forced_constant_refresh;
                let scene_phase = story.active_scene().phase();
                let update_scene = refresh || story.active_scene().in_progress();
                let scene = if scene_phase < 0.5 {
                    if let Some(name) = story.active_scene().from() {
                        story.scene(name)
                    } else {
                        None
                    }
                } else if scene_phase > 0.5 {
                    if let Some(name) = story.active_scene().to() {
                        story.scene(name)
                    } else {
                        None
                    }
                } else {
                    None
                };

                if update_scene {
                    if let Some(alpha) = alphas.get_mut(story_data.overlay) {
                        alpha.0 = (scene_phase * PI).sin();
                    }
                }

                let background_renderable = if let Some(scene) = &scene {
                    if update_scene || scene.background_style.in_progress() {
                        let from = story.background(scene.background_style.from());
                        let to = story.background(scene.background_style.to());
                        if let (Some(from), Some(to)) = (from, to) {
                            let phase = scene.background_style.phase();
                            let transform_prev = {
                                let [a, b, c, d, e, f] = Mat2d::scale(from.scale.into()).0;
                                Command::Transform(a, b, c, d, e, f)
                            };
                            let transform_next = {
                                let [a, b, c, d, e, f] = Mat2d::scale(to.scale.into()).0;
                                Command::Transform(a, b, c, d, e, f)
                            };
                            Some(Renderable::Commands(vec![
                                Command::Store,
                                transform_prev,
                                Command::Alpha(1.0 - phase),
                                Command::Draw(
                                    Image::new_owned(from.image.to_owned())
                                        .align(0.5.into())
                                        .into(),
                                ),
                                Command::Restore,
                                Command::Store,
                                transform_next,
                                Command::Alpha(phase),
                                Command::Draw(
                                    Image::new_owned(to.image.to_owned())
                                        .align(0.5.into())
                                        .into(),
                                ),
                                Command::Restore,
                            ]))
                        } else {
                            Some(Renderable::None)
                        }
                    } else {
                        None
                    }
                } else {
                    Some(Renderable::None)
                };
                if let Some(background_renderable) = background_renderable {
                    if let Some(renderable) = renderables.get_mut(story_data.background) {
                        renderable.0 = background_renderable;
                    }
                }

                if let Some(scene) = &scene {
                    if update_scene
                        || scene.camera_position.in_progress()
                        || scene.camera_rotation.in_progress()
                    {
                        if let Some(transform) = transforms.get_mut(story_data.world_camera) {
                            let position = scene
                                .camera_position
                                .current()
                                .unwrap_or(Default::default());
                            let rotation = scene.camera_rotation.current().unwrap_or(0.0);
                            transform.set_translation(Vec2::new(position.0, position.1));
                            transform.set_rotation(rotation);
                        }
                    }
                }

                for (name, entity) in &story_data.characters {
                    if let Some(character) = story.character(name) {
                        if refresh || character.visibility_anim().in_progress() {
                            if let Some(alpha) = alphas.get_mut(*entity) {
                                alpha.0 = character.visibility();
                            }
                        }
                        if refresh || character.position_anim().in_progress() {
                            if let Some(alignment) = alignments.get_mut(*entity) {
                                let pos = character.position();
                                alignment.1 = Vec2::new(pos.0, pos.1);
                            }
                        }
                        if refresh
                            || character.rotation_anim().in_progress()
                            || character.scale_anim().in_progress()
                        {
                            if let Some(transform) = transforms.get_mut(*entity) {
                                let scl = character.scale();
                                transform.set_rotation(character.rotation());
                                transform.set_scale(Vec2::new(scl.0, scl.1));
                            }
                        }
                        if refresh
                            || character.style_transition().in_progress()
                            || character.alignment_anim().in_progress()
                        {
                            let (from_style, style_phase, to_style) = character.style();
                            let from = character.styles.get(from_style);
                            let to = character.styles.get(to_style);
                            let character_renderable = if let (Some(from), Some(to)) = (from, to) {
                                let align = character.alignment();
                                let align = Vec2::new(align.0, align.1);
                                Renderable::Commands(vec![
                                    Command::Alpha(1.0 - style_phase),
                                    Command::Draw(
                                        Image::new_owned(from.to_owned()).align(align).into(),
                                    ),
                                    Command::Alpha(style_phase),
                                    Command::Draw(
                                        Image::new_owned(to.to_owned()).align(align).into(),
                                    ),
                                ])
                            } else {
                                Renderable::None
                            };
                            if let Some(renderable) = renderables.get_mut(*entity) {
                                renderable.0 = character_renderable;
                            }
                        }
                    } else {
                        if let Some(alpha) = alphas.get_mut(*entity) {
                            alpha.0 = 0.0;
                        }
                    }
                }
            }
        }

        for (transform, alignment) in (&mut transforms, &alignments).join() {
            if let Some(size) = camera_cache.calculate_world_size(alignment.0) {
                transform.set_translation(alignment.1 * size);
            }
        }
    }
}
