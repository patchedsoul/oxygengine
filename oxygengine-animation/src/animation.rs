use crate::{
    curve::{Curved, CurvedDistance, CurvedOffset, PhaseCurve},
    spline::Spline,
};
use core::Scalar;
use serde::{de::DeserializeOwned, Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum AnimationProgression {
    Duration(Scalar),
    Speed(Scalar),
}

impl Default for AnimationProgression {
    fn default() -> Self {
        Self::Speed(1.0)
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
#[serde(bound = "T: Clone")]
pub struct Animation<T>
where
    T: Clone + Default + Curved + CurvedOffset + CurvedDistance + DeserializeOwned + Serialize,
{
    pub value_spline: Spline<T>,
    #[serde(default)]
    pub time_curve: PhaseCurve,
    #[serde(default)]
    pub playing: bool,
    #[serde(default)]
    pub looped: bool,
    #[serde(default)]
    pub progression: AnimationProgression,
    #[serde(default)]
    time: Scalar,
}

impl<T> Animation<T>
where
    T: Clone + Default + Curved + CurvedOffset + CurvedDistance + DeserializeOwned + Serialize,
{
    pub fn new(progression: AnimationProgression, value_spline: Spline<T>) -> Self {
        Self::new_timed(progression, value_spline, Default::default())
    }

    pub fn new_timed(
        progression: AnimationProgression,
        value_spline: Spline<T>,
        time_curve: PhaseCurve,
    ) -> Self {
        Self {
            value_spline,
            time_curve,
            playing: false,
            looped: false,
            progression,
            time: 0.0,
        }
    }

    pub fn instant(value: T, progression: AnimationProgression) -> Self {
        Self::new(
            progression,
            Spline::point(value)
                .expect("Could not create linear value spline for instant animation"),
        )
    }

    pub fn instant_timed(
        value: T,
        progression: AnimationProgression,
        time_curve: PhaseCurve,
    ) -> Self {
        Self::new_timed(
            progression,
            Spline::point(value)
                .expect("Could not create point value spline for instant timed animation"),
            time_curve,
        )
    }

    pub fn sample(&self, time: Scalar) -> Option<T> {
        let factor = match self.progression {
            AnimationProgression::Duration(duration) => {
                if duration > 0.0 {
                    time / duration
                } else {
                    1.0
                }
            }
            AnimationProgression::Speed(_) => {
                let duration = self.value_spline.approx_length();
                if duration > 0.0 {
                    time / duration
                } else {
                    1.0
                }
            }
        };
        let factor = self.time_curve.eval(factor.max(0.0).min(1.0));
        self.value_spline.sample(factor)
    }

    pub fn duration(&self) -> Scalar {
        match self.progression {
            AnimationProgression::Duration(duration) => duration,
            AnimationProgression::Speed(_) => self.value_spline.approx_length(),
        }
    }

    pub fn current(&self) -> Option<T> {
        self.sample(self.time.max(0.0).min(self.duration()))
    }

    pub fn start(&mut self) {
        self.time = 0.0;
    }

    pub fn end(&mut self) {
        self.time = self.duration();
    }

    pub fn set_time(&mut self, time: Scalar) {
        self.time = time.max(0.0).min(self.duration());
    }

    pub fn animate_linear_into(&mut self, value: T) {
        let from = self.value_spline.points().first().unwrap().point.clone();
        self.value_spline = Spline::linear(from, value)
            .expect("Could not create linear value spline to linear animate into value");
    }

    pub fn process(&mut self, delta_time: Scalar) {
        if !self.playing {
            return;
        }
        let duration = self.duration();
        let mut time = self.time + delta_time;
        if time >= duration {
            if self.looped {
                time = 0.0;
            } else {
                self.playing = false;
            }
        }
        self.time = time.max(0.0).min(duration);
    }

    pub fn in_progress(&self) -> bool {
        self.playing
    }

    pub fn is_complete(&self) -> bool {
        !self.in_progress()
    }
}
