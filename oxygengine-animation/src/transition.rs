use crate::curve::PhaseCurve;
use core::Scalar;
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Transition<T>
where
    T: Default + Clone,
{
    from: T,
    to: T,
    #[serde(default)]
    pub curve: PhaseCurve,
    #[serde(default)]
    pub duration: Scalar,
    #[serde(default)]
    time: Scalar,
}

impl<T> Transition<T>
where
    T: Default + Clone,
{
    pub fn new(from: T, to: T, duration: Scalar, curve: PhaseCurve) -> Self {
        Self {
            from,
            to,
            curve,
            duration,
            time: 0.0,
        }
    }

    pub fn instant(value: T) -> Self {
        Self::new(value.clone(), value, 0.0, Default::default())
    }

    pub fn set(&mut self, value: T) {
        self.from = self.to.clone();
        self.to = value;
        self.time = 0.0;
    }

    pub fn end(&mut self) {
        self.time = self.duration;
    }

    pub fn in_progress(&self) -> bool {
        self.time < self.duration
    }

    pub fn is_complete(&self) -> bool {
        !self.in_progress()
    }

    pub fn from(&self) -> &T {
        &self.from
    }

    pub fn to(&self) -> &T {
        &self.to
    }

    pub fn time(&self) -> Scalar {
        self.time
    }

    pub fn phase(&self) -> Scalar {
        if self.duration > 0.0 {
            self.curve.eval(self.time / self.duration)
        } else {
            1.0
        }
    }

    pub fn process(&mut self, delta_time: Scalar) {
        self.time = (self.time + delta_time).max(0.0).min(self.duration);
    }
}
