use core::Scalar;
use serde::{Deserialize, Serialize};
use std::sync::{Arc, RwLock};

pub trait Curved {
    fn interpolate(&self, other: &Self, factor: Scalar) -> Self;
}

impl Curved for Scalar {
    fn interpolate(&self, other: &Self, factor: Scalar) -> Self {
        let diff = other - self;
        diff * factor + self
    }
}

impl Curved for (Scalar, Scalar) {
    fn interpolate(&self, other: &Self, factor: Scalar) -> Self {
        let diff0 = other.0 - self.0;
        let diff1 = other.1 - self.1;
        (diff0 * factor + self.0, diff1 * factor + self.1)
    }
}

impl<T> Curved for Arc<RwLock<T>>
where
    T: Curved,
{
    fn interpolate(&self, other: &Self, factor: Scalar) -> Self {
        let from: &T = &self.read().unwrap();
        let to: &T = &other.read().unwrap();
        let value = from.interpolate(to, factor);
        Arc::new(RwLock::new(value))
    }
}

pub trait CurvedDistance {
    fn curved_distance(&self, other: &Self) -> Scalar;
}

impl CurvedDistance for Scalar {
    fn curved_distance(&self, other: &Self) -> Scalar {
        other - self
    }
}

impl CurvedDistance for (Scalar, Scalar) {
    fn curved_distance(&self, other: &Self) -> Scalar {
        let diff0 = other.0 - self.0;
        let diff1 = other.1 - self.1;
        (diff0 * diff0 + diff1 * diff1).sqrt()
    }
}

impl<T> CurvedDistance for Arc<RwLock<T>>
where
    T: CurvedDistance,
{
    fn curved_distance(&self, other: &Self) -> Scalar {
        let from: &T = &self.read().unwrap();
        let to: &T = &other.read().unwrap();
        from.curved_distance(to)
    }
}

pub trait CurvedOffset {
    fn curved_offset(&self, other: &Self, forward: bool) -> Self;
}

impl CurvedOffset for Scalar {
    fn curved_offset(&self, other: &Self, forward: bool) -> Self {
        if forward {
            self + other
        } else {
            self - other
        }
    }
}

impl CurvedOffset for (Scalar, Scalar) {
    fn curved_offset(&self, other: &Self, forward: bool) -> Self {
        if forward {
            (self.0 + other.0, self.1 + other.1)
        } else {
            (self.0 - other.0, self.1 - other.1)
        }
    }
}

impl<T> CurvedOffset for Arc<RwLock<T>>
where
    T: CurvedOffset,
{
    fn curved_offset(&self, other: &Self, forward: bool) -> Self {
        let from: &T = &self.read().unwrap();
        let to: &T = &other.read().unwrap();
        Arc::new(RwLock::new(from.curved_offset(to, forward)))
    }
}

pub trait CurvedDirection {
    fn curved_direction(&self, other: &Self) -> Self;
}

impl CurvedDirection for Scalar {
    fn curved_direction(&self, other: &Self) -> Self {
        other - self
    }
}

impl CurvedDirection for (Scalar, Scalar) {
    fn curved_direction(&self, other: &Self) -> Self {
        (other.0 - self.0, other.1 - self.1)
    }
}

impl<T> CurvedDirection for Arc<RwLock<T>>
where
    T: CurvedDirection,
{
    fn curved_direction(&self, other: &Self) -> Self {
        let from: &T = &self.read().unwrap();
        let to: &T = &other.read().unwrap();
        Arc::new(RwLock::new(from.curved_direction(to)))
    }
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub struct Curve<T>
where
    T: Clone + Curved,
{
    pub from: T,
    pub to: T,
    pub from_param: T,
    pub to_param: T,
}

impl<T> Curve<T>
where
    T: Clone + Curved,
{
    pub fn linear(from: T, to: T) -> Self {
        Self {
            from: from.clone(),
            to: to.clone(),
            from_param: from,
            to_param: to,
        }
    }

    pub fn bezier(from: T, to: T, from_param: T, to_param: T) -> Self {
        Self {
            from,
            to,
            from_param,
            to_param,
        }
    }

    pub fn sample(&self, factor: Scalar) -> T {
        let a = self.from.interpolate(&self.from_param, factor);
        let b = self.from_param.interpolate(&self.to_param, factor);
        let c = self.to_param.interpolate(&self.to, factor);
        let d = a.interpolate(&b, factor);
        let e = b.interpolate(&c, factor);
        d.interpolate(&e, factor)
    }

    pub fn sample_direction_with_sensitivity(&self, factor: Scalar, sensitivity: Scalar) -> T
    where
        T: Default + CurvedDirection + CurvedDistance,
    {
        let approx_length = self.approx_length();
        if approx_length > 0.0 {
            let s = sensitivity / approx_length;
            let a = self.sample(factor - s);
            let b = self.sample(factor + s);
            a.curved_direction(&b)
        } else {
            T::default()
        }
    }

    pub fn sample_direction(&self, factor: Scalar) -> T
    where
        T: Default + CurvedDirection + CurvedDistance,
    {
        self.sample_direction_with_sensitivity(factor, 1.0e-2)
    }

    pub fn approx_length(&self) -> Scalar
    where
        T: CurvedDistance,
    {
        let a = self.from.curved_distance(&self.to);
        let b = self.from.curved_distance(&self.from_param);
        let c = self.from_param.curved_distance(&self.to_param);
        let d = self.to_param.curved_distance(&self.to);
        (a + b + c + d) * 0.5
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct PhaseCurveDef(pub Scalar, pub Scalar, pub Scalar, pub Scalar);

impl Default for PhaseCurveDef {
    fn default() -> Self {
        Self(0.0, 0.0, 1.0, 1.0)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(from = "PhaseCurveDef")]
#[serde(into = "PhaseCurveDef")]
pub struct PhaseCurve {
    params: (Scalar, Scalar, Scalar, Scalar),
    cached: Curve<(Scalar, Scalar)>,
}

impl Default for PhaseCurve {
    fn default() -> Self {
        Self::linear()
    }
}

impl PhaseCurve {
    pub fn linear() -> Self {
        let params = (0.0, 0.0, 1.0, 1.0);
        let cached = Self::build_curve(&params);
        Self { params, cached }
    }

    pub fn bezier(p0: Scalar, p1: Scalar, p2: Scalar, p3: Scalar) -> Self {
        let params = (p0, p1, p2, p3);
        let cached = Self::build_curve(&params);
        Self { params, cached }
    }

    pub fn params(&self) -> &(Scalar, Scalar, Scalar, Scalar) {
        &self.params
    }

    pub fn eval(&self, mut factor: Scalar) -> Scalar {
        factor = factor.max(0.0).min(1.0);
        self.cached.sample(factor).1
    }

    fn build_curve(params: &(Scalar, Scalar, Scalar, Scalar)) -> Curve<(Scalar, Scalar)> {
        Curve::bezier(
            (0.0, 0.0),
            (1.0, 1.0),
            (params.0.max(0.0).min(1.0), params.1),
            (params.2.max(0.0).min(1.0), params.3),
        )
    }
}

impl From<PhaseCurveDef> for PhaseCurve {
    fn from(value: PhaseCurveDef) -> Self {
        Self::bezier(value.0, value.1, value.2, value.3)
    }
}

impl Into<PhaseCurveDef> for PhaseCurve {
    fn into(self) -> PhaseCurveDef {
        PhaseCurveDef(self.params.0, self.params.1, self.params.2, self.params.3)
    }
}
