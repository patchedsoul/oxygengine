use crate::curve::*;
use core::Scalar;
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use std::{convert::TryFrom, fmt};

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
#[serde(bound = "T: Clone")]
pub struct SplinePoint<T>
where
    T: Clone + Default + Curved + DeserializeOwned + Serialize,
{
    pub point: T,
    #[serde(default)]
    pub direction: T,
}

impl<T> SplinePoint<T>
where
    T: Clone + Default + Curved + DeserializeOwned + Serialize,
{
    pub fn point(point: T) -> Self {
        Self {
            point,
            direction: Default::default(),
        }
    }

    pub fn new(point: T, direction: T) -> Self {
        Self { point, direction }
    }
}

impl<T> From<T> for SplinePoint<T>
where
    T: Clone + Default + Curved + DeserializeOwned + Serialize,
{
    fn from(value: T) -> Self {
        Self::point(value)
    }
}

impl<T> From<(T, T)> for SplinePoint<T>
where
    T: Clone + Default + Curved + DeserializeOwned + Serialize,
{
    fn from(value: (T, T)) -> Self {
        Self::new(value.0, value.1)
    }
}

impl<T> From<[T; 2]> for SplinePoint<T>
where
    T: Clone + Default + Curved + DeserializeOwned + Serialize,
{
    fn from(value: [T; 2]) -> Self {
        let [a, b] = value;
        Self::new(a, b)
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum SplineError {
    EmptyPointsList,
}

impl fmt::Display for SplineError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

pub type SplineDef<T> = Vec<SplinePoint<T>>;

#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(try_from = "SplineDef<T>")]
#[serde(into = "SplineDef<T>")]
#[serde(bound = "T: Clone")]
pub struct Spline<T>
where
    T: Clone + Default + Curved + CurvedDistance + CurvedOffset + DeserializeOwned + Serialize,
{
    points: Vec<SplinePoint<T>>,
    cached: Vec<Curve<T>>,
    approx_length: Scalar,
    parts_approx_times: Vec<Scalar>,
}

impl<T> Default for Spline<T>
where
    T: Clone + Default + Curved + CurvedDistance + CurvedOffset + DeserializeOwned + Serialize,
{
    fn default() -> Self {
        Self::point(T::default()).unwrap()
    }
}

impl<T> Spline<T>
where
    T: Clone + Default + Curved + CurvedDistance + CurvedOffset + DeserializeOwned + Serialize,
{
    pub fn new(mut points: Vec<SplinePoint<T>>) -> Result<Self, SplineError> {
        if points.len() == 0 {
            return Err(SplineError::EmptyPointsList);
        }
        if points.len() == 1 {
            points.push(points[0].clone())
        }
        let cached = points
            .windows(2)
            .map(|pair| {
                let from_param = pair[0].point.curved_offset(&pair[0].direction, true);
                let to_param = pair[1].point.curved_offset(&pair[1].direction, false);
                Curve::bezier(
                    pair[0].point.clone(),
                    pair[1].point.clone(),
                    from_param,
                    to_param,
                )
            })
            .collect::<Vec<_>>();
        let lengths = cached
            .iter()
            .map(|curve| curve.approx_length())
            .collect::<Vec<_>>();
        let mut time = 0.0;
        let mut parts_approx_times = Vec::with_capacity(points.len());
        parts_approx_times.push(0.0);
        for length in &lengths {
            time = time + length;
            parts_approx_times.push(time);
        }
        Ok(Self {
            points,
            cached,
            approx_length: time,
            parts_approx_times,
        })
    }

    pub fn linear(from: T, to: T) -> Result<Self, SplineError> {
        Self::new(vec![SplinePoint::point(from), SplinePoint::point(to)])
    }

    pub fn point(point: T) -> Result<Self, SplineError> {
        Self::linear(point.clone(), point)
    }

    pub fn sample(&self, factor: Scalar) -> Option<T> {
        if self.cached.is_empty() {
            return None;
        }
        let t = factor * self.approx_length;
        let index = match self
            .parts_approx_times
            .binary_search_by(|time| time.partial_cmp(&t).unwrap())
        {
            Ok(index) | Err(index) => index,
        };
        let index = index.min(self.cached.len() - 1);
        let a = self.parts_approx_times[index];
        let length = self.parts_approx_times[index + 1] - a;
        let f = if length > 0.0 { (t - a) / length } else { 1.0 };
        Some(self.cached[index].sample(f))
    }

    pub fn sample_direction_with_sensitivity(
        &self,
        factor: Scalar,
        sensitivity: Scalar,
    ) -> Option<T>
    where
        T: CurvedDirection,
    {
        let s = sensitivity / self.approx_length;
        let a = self.sample(factor - s)?;
        let b = self.sample(factor + s)?;
        Some(a.curved_direction(&b))
    }

    pub fn sample_direction(&self, factor: Scalar) -> Option<T>
    where
        T: CurvedDirection,
    {
        self.sample_direction_with_sensitivity(factor, 1.0e-2)
    }

    pub fn approx_length(&self) -> Scalar {
        self.approx_length
    }

    pub fn points(&self) -> &[SplinePoint<T>] {
        &self.points
    }

    pub fn curves(&self) -> &[Curve<T>] {
        &self.cached
    }
}

impl<T> TryFrom<SplineDef<T>> for Spline<T>
where
    T: Clone + Default + Curved + CurvedDistance + CurvedOffset + DeserializeOwned + Serialize,
{
    type Error = SplineError;

    fn try_from(value: SplineDef<T>) -> Result<Self, Self::Error> {
        Self::new(value)
    }
}

impl<T> Into<SplineDef<T>> for Spline<T>
where
    T: Clone + Default + Curved + CurvedDistance + CurvedOffset + DeserializeOwned + Serialize,
{
    fn into(self) -> SplineDef<T> {
        self.points
    }
}
